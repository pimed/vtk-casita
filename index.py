from src import Engine
from src import WorldObject

engine = Engine.Engine("VTK-casita", 800, 600)
baseAtrib = {'SetXLength':30, 'SetYLength':1, 'SetZLength':30}
base = engine.addWorldObject("cube", baseAtrib)
base.setColor(0.1, 0.3, 0.2)
base.setPosition(0, 0, 0)

# Front towers
towerFrontAtrib = {'SetRadius':1.2,'SetHeight':2.5,'SetResolution':50}
towerFrontRight = engine.addWorldObject("cylinder", towerFrontAtrib)
towerFrontRight.setPosition(2.5, 1.8, 4.8)
towerFrontLeft = engine.addWorldObject("cylinder", towerFrontAtrib)
towerFrontLeft.setPosition(-2.5, 1.8, 4.8)

towerFrontCrownAtrib = {'SetRadius':1.5,'SetHeight':0.7,'SetResolution':50}
towerFrontRightCrown = engine.addWorldObject("cylinder", towerFrontCrownAtrib)
towerFrontRightCrown.setPosition(2.5, 3.2, 4.8)
towerFrontRightCrown.setColor(0.46, 0.46, 0.46)
towerFrontLeftCrown = engine.addWorldObject("cylinder", towerFrontCrownAtrib)
towerFrontLeftCrown.setPosition(-2.5, 3.2, 4.8) 
towerFrontLeftCrown.setColor(0.46, 0.46, 0.46)

# Side towers 
towerSideAtrib = {'SetRadius':1,'SetHeight':5,'SetResolution':50}
towerSideRight = engine.addWorldObject("cylinder", towerSideAtrib)
towerSideRight.setPosition(5, 3, 0)
towerSideLeft = engine.addWorldObject("cylinder", towerSideAtrib)
towerSideLeft.setPosition(-5, 3, 0)

towerSideCrownAtrib = {'SetRadius':1.2,'SetHeight':0.7,'SetResolution':50}
towerSideRightCrown = engine.addWorldObject("cylinder", towerSideCrownAtrib)
towerSideRightCrown.setPosition(5, 5.5, 0)
towerSideRightCrown.setColor(0.46, 0.46, 0.46)
towerSideLeftCrown = engine.addWorldObject("cylinder", towerSideCrownAtrib)
towerSideLeftCrown.setPosition(-5, 5.5, 0)
towerSideLeftCrown.setColor(0.46, 0.46, 0.46)

towerSideBalconyAtrib = {'SetRadius':1.8,'SetHeight':1,'SetResolution':50}
towerSideLeftBalcony = engine.addWorldObject("cylinder", towerSideBalconyAtrib)
towerSideLeftBalcony.setPosition(-5, 3.5, 0)

towerSideConeAtrib = {'SetRadius':1,'SetHeight':3, 'SetResolution':50, 'SetDirection':(0, 1, 0)}
towerSideRightCone = engine.addWorldObject("cone", towerSideConeAtrib)
towerSideRightCone.setPosition(5, 7, 0)
towerSideRightCone.setColor(0.53, 0.07, 0.09)
towerSideLeftCone = engine.addWorldObject("cone", towerSideConeAtrib)
towerSideLeftCone.setPosition(-5, 7, 0)
towerSideLeftCone.setColor(0.53, 0.07, 0.09)

# Back towers
towerBackAtribRight = {'SetRadius':1,'SetHeight':8,'SetResolution':50}
towerBackRight = engine.addWorldObject("cylinder", towerBackAtribRight)
towerBackRight.setPosition(3, 4, -5)
towerBackAtribLeft = {'SetRadius':1,'SetHeight':9,'SetResolution':50}
towerBackLeft = engine.addWorldObject("cylinder", towerBackAtribLeft)
towerBackLeft.setPosition(-3, 5, -5)

towerBackCrownAtrib = {'SetRadius':1.2,'SetHeight':0.7,'SetResolution':50}
towerBackRightCrown = engine.addWorldObject("cylinder", towerBackCrownAtrib)
towerBackRightCrown.setPosition(3, 8, -5)
towerBackRightCrown.setColor(0.46, 0.46, 0.46)
towerBackLeftCrown = engine.addWorldObject("cylinder", towerBackCrownAtrib)
towerBackLeftCrown.setPosition(-3, 9.5, -5)
towerBackLeftCrown.setColor(0.46, 0.46, 0.46)

towerBackConeAtrib = {'SetRadius':1,'SetHeight':3, 'SetResolution':50, 'SetDirection':(0, 1, 0)}
towerBackRightCone = engine.addWorldObject("cone", towerBackConeAtrib)
towerBackRightCone.setPosition(3, 9.5, -5)
towerBackRightCone.setColor(0.53, 0.07, 0.09)
towerBackLeftCone = engine.addWorldObject("cone", towerBackConeAtrib)
towerBackLeftCone.setPosition(-3, 11, -5)
towerBackLeftCone.setColor(0.53, 0.07, 0.09)


# Tower central
towerCentralAtrib = {'SetRadius':2,'SetHeight':5,'SetResolution':50}
towerCentral = engine.addWorldObject("cylinder", towerCentralAtrib)
towerCentral.setPosition(0, 3, 0)

towerCentralCrownAtrib = {'SetRadius':2.5,'SetHeight':0.7,'SetResolution':50}
towerCentralCrown = engine.addWorldObject("cylinder", towerCentralCrownAtrib)
towerCentralCrown.setPosition(0, 5.2, 0)
towerCentralCrown.setColor(0.46, 0.46, 0.46)

towerCentralConeAtrib = {'SetRadius':2,'SetHeight':3, 'SetResolution':50, 'SetDirection':(0, 1, 0)}
towerCentralCone = engine.addWorldObject("cone", towerCentralConeAtrib)
towerCentralCone.setPosition(0, 7, 0)
towerCentralCone.setColor(0.53, 0.07, 0.09)

# Door
door = {'SetXLength':4, 'SetYLength':2, 'SetZLength':1}
door = engine.addWorldObject("cube", door)
door.setColor(0.70, 0.70, 0.70)
door.setPosition(0, 1.5, 5)

doorAtrib = {'SetXLength':1, 'SetYLength':1.5, 'SetZLength':0.1}
doorLeft = engine.addWorldObject("cube", doorAtrib)
doorLeft.setColor(0.53, 0.07, 0.09)
doorLeft.setPosition(-0.5, 1, 5.6)
doorRight = engine.addWorldObject("cube", doorAtrib)
doorRight.setColor(0.53, 0.07, 0.09)
doorRight.setPosition(0.51, 1, 5.6)


# Chapa
chapaAtrib = {'SetRadius':0.15}
chapaLeft = engine.addWorldObject("sphere", chapaAtrib)
chapaLeft.setColor(0.70, 0.70, 0.70)
chapaLeft.setPosition(-0.2, 1, 5.6)
chapaRight = engine.addWorldObject("sphere", chapaAtrib)
chapaRight.setColor(0.70, 0.70, 0.70)
chapaRight.setPosition(0.21, 1, 5.6)


# Cloud
cloudAtrib = {'SetRadius':0.9}
cloudRight1 = engine.addWorldObject("sphere", cloudAtrib)
cloudRight1.setColor(1, 1, 1)
cloudRight1.setPosition(7, 18, 7)
cloudRight2 = engine.addWorldObject("sphere", cloudAtrib)
cloudRight2.setColor(1, 1, 1)
cloudRight2.setPosition(8, 18, 7)
cloudRight3 = engine.addWorldObject("sphere", cloudAtrib)
cloudRight3.setColor(1, 1, 1)
cloudRight3.setPosition(9, 18, 7)
cloudRight4 = engine.addWorldObject("sphere", cloudAtrib)
cloudRight4.setColor(1, 1, 1)
cloudRight4.setPosition(7.5, 18.5, 7)
cloudRight5 = engine.addWorldObject("sphere", cloudAtrib)
cloudRight5.setColor(1, 1, 1)
cloudRight5.setPosition(8.5, 18.5, 7)

cloudLeft1 = engine.addWorldObject("sphere", cloudAtrib)
cloudLeft1.setColor(1, 1, 1)
cloudLeft1.setPosition(-7, 15, 7)
cloudLeft2 = engine.addWorldObject("sphere", cloudAtrib)
cloudLeft2.setColor(1, 1, 1)
cloudLeft2.setPosition(-8, 15, 7)
cloudLeft3 = engine.addWorldObject("sphere", cloudAtrib)
cloudLeft3.setColor(1, 1, 1)
cloudLeft3.setPosition(-9, 15, 7)
cloudLeft4 = engine.addWorldObject("sphere", cloudAtrib)
cloudLeft4.setColor(1, 1, 1)
cloudLeft4.setPosition(-7.5, 15.5, 7)
cloudLeft5 = engine.addWorldObject("sphere", cloudAtrib)
cloudLeft5.setColor(1, 1, 1)
cloudLeft5.setPosition(-8.5, 15.5, 7)


# Front walls
wallFrontAttrib = {'SetXLength':5.5, 'SetYLength':2, 'SetZLength':1}
wallFrontRight = engine.addWorldObject("cube", wallFrontAttrib, 63)
wallFrontRight.setColor(0.70, 0.70, 0.70)
wallFrontRight.setPosition(4, 1.5, 2.8)

wallFrontLeft = engine.addWorldObject("cube", wallFrontAttrib, -63)
wallFrontLeft.setColor(0.70, 0.70, 0.70)
wallFrontLeft.setPosition(-4, 1.5, 2.8)

# Back walls
wallBackAttrib = {'SetXLength':5, 'SetYLength':2, 'SetZLength':1}
wallBackRight = engine.addWorldObject("cube", wallBackAttrib, 68)
wallBackRight.setPosition(-4, 1.5, -3)
wallBackRight.setColor(0.70, 0.70, 0.70)

wallBackLeft = engine.addWorldObject("cube", wallBackAttrib, -68)
wallBackLeft.setPosition(4, 1.5, -3)
wallBackLeft.setColor(0.70, 0.70, 0.70)

# Last wall
doorBackAttrib = {'SetXLength':5, 'SetYLength':2, 'SetZLength':1}
doorBack = engine.addWorldObject("cube", doorBackAttrib)
doorBack.setPosition(0, 1.5, -5)
doorBack.setColor(0.70, 0.70, 0.70)

# Bush
#bushAtrib = {'SetXLength':1, 'SetYLength':1, 'SetZLength':1}
#bush0 = engine.addWorldObject("cube", bushAtrib)
#bush0.setPosition(5, 1, 5)
#bush0.setColor(0.21, 0.47, 0.17)

# Tree
treeAtrib = {'SetXLength':0.2, 'SetYLength':2, 'SetZLength':0.2}
tree = engine.addWorldObject("cube", treeAtrib)
tree.setPosition(-7, 1, 5)
tree.setColor(0.57, 0.32, 0.13)
tree2 = engine.addWorldObject("cube", treeAtrib)
tree2.setPosition(7, 1, 5)
tree2.setColor(0.57, 0.32, 0.13)
tree3 = engine.addWorldObject("cube", treeAtrib)
tree3.setPosition(-9, 1, -7)
tree3.setColor(0.57, 0.32, 0.13)
tree4 = engine.addWorldObject("cube", treeAtrib)
tree4.setPosition(9, 1, -7)
tree4.setColor(0.57, 0.32, 0.13)

treeLeafAtrib0 = {'SetRadius':1,'SetHeight':0.3,'SetResolution':50}
treeLeaf0 = engine.addWorldObject("cylinder", treeLeafAtrib0)
treeLeaf0.setPosition(-7, 1, 5)
treeLeaf0.setColor(0.21, 0.47, 0.17)
treeRigth0 = engine.addWorldObject("cylinder", treeLeafAtrib0)
treeRigth0.setPosition(7, 1, 5)
treeRigth0.setColor(0.21, 0.47, 0.17)
treeLeafDown0 = engine.addWorldObject("cylinder", treeLeafAtrib0)
treeLeafDown0.setPosition(-9, 1, -7)
treeLeafDown0.setColor(0.21, 0.47, 0.17)
treeRigtDown0 = engine.addWorldObject("cylinder", treeLeafAtrib0)
treeRigtDown0.setPosition(9, 1, -7)
treeRigtDown0.setColor(0.21, 0.47, 0.17)


treeLeafAtrib1 = {'SetRadius':0.8,'SetHeight':0.3,'SetResolution':50}
treeLeaf1 = engine.addWorldObject("cylinder", treeLeafAtrib1)
treeLeaf1.setPosition(-7, 1.5, 5)
treeLeaf1.setColor(0.21, 0.47, 0.17)
treeRigth1 = engine.addWorldObject("cylinder", treeLeafAtrib1)
treeRigth1.setPosition(7, 1.5, 5)
treeRigth1.setColor(0.21, 0.47, 0.17)
treeLeafDown1 = engine.addWorldObject("cylinder", treeLeafAtrib1)
treeLeafDown1.setPosition(-9, 1.5, -7)
treeLeafDown1.setColor(0.21, 0.47, 0.17)
treeRigthDown1 = engine.addWorldObject("cylinder", treeLeafAtrib1)
treeRigthDown1.setPosition(9, 1.5, -7)
treeRigthDown1.setColor(0.21, 0.47, 0.17)

treeLeafAtrib2 = {'SetRadius':0.5,'SetHeight':0.3,'SetResolution':50}
treeLeaf2 = engine.addWorldObject("cylinder", treeLeafAtrib2)
treeLeaf2.setPosition(-7, 2, 5)
treeLeaf2.setColor(0.21, 0.47, 0.17)
treeRigth2 = engine.addWorldObject("cylinder", treeLeafAtrib2)
treeRigth2.setPosition(7, 2, 5)
treeRigth2.setColor(0.21, 0.47, 0.17)
treeLeafDown2 = engine.addWorldObject("cylinder", treeLeafAtrib2)
treeLeafDown2.setPosition(-9, 2, -7)
treeLeafDown2.setColor(0.21, 0.47, 0.17)
treeRigthDown2 = engine.addWorldObject("cylinder", treeLeafAtrib2)
treeRigthDown2.setPosition(9, 2, -7)
treeRigthDown2.setColor(0.21, 0.47, 0.17)

#casita
casaAtrib = {'SetXLength':1, 'SetYLength':1, 'SetZLength':1}
casa = engine.addWorldObject("cube", casaAtrib)
casa.setColor(0.82, 0.76, 0.40)
casa.setPosition(-10, 1, 6)

puertaCasaAtrib = {'SetXLength':0.5, 'SetYLength':0.65, 'SetZLength':0.5}
puertaCasa = engine.addWorldObject("cube", puertaCasaAtrib)
puertaCasa.setColor(0.63, 0.39, 0.11)
puertaCasa.setPosition(-10, 0.85, 6.30)

chapaCasaAtrib = {'SetRadius':0.08}
chapaCasa = engine.addWorldObject("sphere", chapaCasaAtrib)
chapaCasa.setColor(0.70, 0.70, 0.70)
chapaCasa.setPosition(-10.18, 0.85, 6.5)

techoCasaAtrib = {'SetRadius':0.8,'SetHeight':1, 'SetResolution':50, 'SetDirection':(0, 1, 0)}
techoCasa = engine.addWorldObject("cone", techoCasaAtrib)
techoCasa.setPosition(-10, 2, 6)
techoCasa.setColor(0.63, 0.39, 0.11)

ventanaCasaAtrib = {'SetXLength':0.5, 'SetYLength':0.5, 'SetZLength':0.5}
ventanaCasa = engine.addWorldObject("cube", ventanaCasaAtrib)
ventanaCasa.setPosition(-10.3, 1, 6)
ventanaCasa.setColor(0.64, 0.70, 0.79)

engine.startLoop()