import vtk
from src import WorldObject
from src import ShapeFactory

class Engine:
    def __init__(self, windowName, width, height):
        self.renderer = vtk.vtkRenderer()
        self.renderer.SetBackground(0.60, 0.65, 0.92)

        self.window = vtk.vtkRenderWindow()
        self.window.SetWindowName(windowName)
        self.window.SetSize(width, height)
        self.window.AddRenderer(self.renderer)

        self.interactor = vtk.vtkRenderWindowInteractor()
        self.interactor.SetRenderWindow(self.window)
        self.interactor.Initialize()

        self.shapeFactory = ShapeFactory.ShapeFactory()

    def startLoop(self):
        self.window.Render()
        self.interactor.Start()

    def addWorldObjectToRenderer(self, object):
        self.renderer.AddActor(object.actor)
        self.renderer.AddActor(object.transActor)

    def addWorldObject(self, shape, attributes, angle = 0):
        newShape = self.shapeFactory.createNewShape(shape, attributes)
        newShape = WorldObject.WorldObject(newShape, angle)
        self.addWorldObjectToRenderer(newShape)
        return newShape