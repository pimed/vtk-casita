import vtk

class ShapeFactory:
    def __init__(self):
        self.shapes = {
            "cone" : "vtkConeSource",
            "cube" : "vtkCubeSource",
            "sphere": "vtkSphereSource",
            "cylinder": "vtkCylinderSource",
            "plane": "vtkPlaneSource"
        } 

    def createNewShape(self, shape, attributes):
        methodToCall = self.shapes.get(shape)
        newShape = getattr(vtk, methodToCall)()
        self.addAttributes(newShape, attributes)
        newShape.Update()
        return newShape

    def addAttributes(self, shape, attributes):
        for attribute in attributes:
            getattr(shape,attribute)(attributes[attribute])
