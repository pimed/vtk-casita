import vtk

class Mapper:
    def __init__(self, primitiveObject):
        self.data = vtk.vtkPolyDataMapper()
        self.data.SetInputData(primitiveObject.GetOutput())