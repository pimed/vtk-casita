import vtk
from src import Mapper

class WorldObject:
    def __init__ (self, objectToMap, angle = 0):
        transform = vtk.vtkTransform()
        transform.RotateWXYZ(angle, 0, 1, 0)
        transformFilter=vtk.vtkTransformPolyDataFilter()
        transformFilter.SetTransform(transform)
        transformFilter.SetInputConnection(objectToMap.GetOutputPort())
        transformFilter.Update()

        self.source = objectToMap
        self.mapper = Mapper.Mapper(objectToMap) #create an object from factory(primitive)
        self.transMapper = Mapper.Mapper(transformFilter)
        self.actor = vtk.vtkActor()
        self.transActor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper.data)
        self.actor.SetMapper(self.transMapper.data)
    
    def setPosition(self, axisX, axisY, axisZ):
        self.actor.SetPosition(axisX, axisY, axisZ) 
    
    def setColor(self, red, green, blue):
        self.actor.GetProperty().SetColor(red, green, blue)